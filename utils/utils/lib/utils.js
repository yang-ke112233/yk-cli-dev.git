"use strict";
const chidProcess = require("child_process");
function isObject(data) {
  return Object.prototype.toString.call(data) === "[object Object]";
}
function spinnerStart(msg, spinnerString = "|/-\\") {
  const Spinner = require("cli-spinner").Spinner;
  const spinner = new Spinner(msg + " %s");
  spinner.setSpinnerString(spinnerString);
  spinner.start();
  return spinner;
}
function sleep(timeout = 1000) {
  return new Promise((resolve) => setTimeout(resolve, timeout));
}
function spawn(command, args, options) {
  const win32 = process.platform === "win32";
  const cmd = win32 ? "cmd" : command;
  const commandArgs = win32 ? ["/c"].concat(command, args) : args;
  return chidProcess.spawn(cmd, commandArgs, options || {});
}
//安装命令函数
function execAsync(command, args, options) {
    return new Promise((resolve, reject) =>{
      const cp = spawn(command, args, options);
      cp.on("error", (e) => {
        reject(e)
      })
      cp.on("exit",c=>{
        resolve(c)
      })
    })
}
module.exports = {
  isObject,
  spinnerStart,
  sleep,
  spawn,
  execAsync
};
