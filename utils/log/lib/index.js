"use strict";

const log = require("npmlog");
log.level = process.env.LOG_LEVEL || "info"; // 设置日志级别
log.addLevel("success", 2000, { fg: "green", bold: true }); //添加自定义命令
log.heading = "yk-cli-dev";//修改前缀
log.headingStyle = { fg: "blue", bold: true, bg: "white" };
module.exports = log;
