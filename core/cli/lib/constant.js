const LOWEST_NODE_VERSION="18.17.0"
const DEFAULT_CLI_HOME=".yk-cli-dev"
const BASE_URL="http://localhost/:7002"
module.exports=
{
  LOWEST_NODE_VERSION,
  DEFAULT_CLI_HOME,
  BASE_URL
}